PHP-FPM image for running a Laravel app
=======================================

Because we need some additional php modules
on top of the default image.

Running
-------
Specify the debugger client address as an environment variable:

    XDEBUG_CONFIG=remote_host=docker.for.mac.localhost

Building
--------

Building Docker image:
  
    docker build -t registry.gitlab.com/wysiwygoy/dev/php-fpm-laravel .
